O que fazer para tudo rodar corretamente (em ordem):

- yarn init (cria o package.json)
- yarn add express (cria node_modules e yarn.lock)
- yarn add nodemon (obs.: add no package.json: "scripts" {
  "dev": "nodemon src/server.js"
  })
- yarn add uuid (sobe o servidor)
- yarn add swagger-ui-express (obs.: add no server.js: const swaggerFile = require("./swagger.json"); E app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));)
- yarn add sequelize (obs.: criar config/database.js)
- yarn add sqlite3
- criar createDataBase e add no server.js (cria database.sqlite)
