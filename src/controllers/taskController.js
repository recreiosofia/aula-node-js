const { Op } = require("sequelize");

const { Task } = require("../models/models");

//Criar Tarefa
async function createTask(req, res) {
  try {
        const { user } = req;

        const { name, deadline, description } = req.body;

        const task = await Task.create({
            name,
            description,
            deadline: new Date(deadline),
            UserId: user.id,
        });

        return res.status(201).json(task);
    } catch (error) {
        return res.status(400).json({ error: "Não foi possível criar a tarefa" });
    }
}

//Buscar tarefa pelo Id
function getTaskById(req, res) {
    const { task } = req;

    return res.status(200).json(task);
}

//Atualizar tarefa (se ela já foi concluída ou não)
async function updateTask(req, res) {
    try {
        const { task } = req;

        const taskUpdated = await task.update({ done: true });

        return res.status(200).json(taskUpdated);
    } catch (error) {
        return res.status(500).json({ error: "Erro ao atualizar a tarefa" });
    }
}

//Deletar Tarefa
async function deleteTask(req, res) {
    try {
        const { task } = req;

        await task.destroy();

        return res.status(204).end();
    } catch (error) {
        return res.status(500).json({ error: "Erro ao deletar o usuário" });
    }
}

//Listar tarefas pendentes
async function listPendingTasks(req, res) {
    try {
        const { user } = req;

        const pendingTasks = await Task.findAll({
            where: {
                [Op.and]: {
                    UserId: user.id,
                    deadline: {
                        [Op.gte]: new Date(),
                    },
                    done: false,
                },
            },
        });

        if (!pendingTasks) {
            return res.status(404).json({ error: "Pending Tasks not Found" });
        }

        return res.status(200).json(pendingTasks);
    } catch (error) {
        return res.status(500).json({ error: "Erro ao listar tarefas" });
    }
}

module.exports = {
  createTask,
  getTaskById,
  updateTask,
  deleteTask,
  listPendingTasks,
};
