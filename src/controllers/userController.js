const users = [];

const { User } = require("../models/models");

//Criar um usuário
async function createUser(req, res) {
  try {
    const { username } = req.body;

    const user =  await User.create({
      username,
    });

    users.push(user);
    return res.status(201).json(user);
  } catch (error) {
    return res.status(400).json({ error: "Não foi possível criar o usuário" });
  }
}

//Listar usuários
function listUsers(req, res) {
  return res.status(200).json(users);
}

//Buscar usuário pelo id
function getUser(req, res) {
  const { user } = req;

  return res.status(200).json(user);
}

//Atualizar nome do usuário
async function updateUser(req, res) {
  try {
    const { user } = req;

    const { username } = req.body;

    const userUpdated = await user.update({ username });

    return res.status(200).json(userUpdated);
  } catch (error) {
    return res.status(500).json({ error: "Erro ao atualizar o usuário" });
  }
}

//Tornar usuário admin
async function makeUserAdmin(req, res) {
  try {
    const { user } = req;

    if (user.isAdmin === true) {
      return res.status(400).json({ error: "User is already an admin!" });
    }

    const userUpdated = await user.update({ isAdmin: true });

    return res.status(200).json(userUpdated);
  } catch (error) {
    return res.status(500).json({ error: "Erro ao tornar usuário admin" });
  }
}

//Deletar usuário
async function DeleteUser(req, res) {
  try {
    const { user } = req;

    await user.destroy();

    return res.status(204).end();
  } catch (error) {
    return res.status(500).json({ error: "Erro ao deletar o usuário" });
  }
}

module.exports = {
  createUser,
  listUsers,
  getUser,
  updateUser,
  makeUserAdmin,
  DeleteUser,
  users,
};
