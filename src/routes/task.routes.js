const express = require('express');
const taskRoutes = express.Router();

//Inclusão dos Middlewares
const findTask = require("../middlewares/findTaskMiddleware");
const checkUserTaskLimit = require("../middlewares/checkUserTaskLimitMiddleware");
const findUser = require("../middlewares/FindUserMiddleware");

//Inclusão dos Controllers
const taskController = require("../controllers/taskController");

//Criar Tarefa
taskRoutes.post('/:userId', findUser, checkUserTaskLimit, (req, res) =>
   taskController.createTask(req,res),
);

//Buscar tarefa pelo Id
taskRoutes.get('/:userId/:taskId', findUser, findTask, (req, res) => 
   taskController.getTaskById(req,res),
);

//Atualizar tarefa (se ela já foi concluída ou não)
taskRoutes.patch('/:userId/:taskId', findUser, findTask, (req, res) =>
  taskController.updateTask(req, res),
);

//Deletar Tarefa
taskRoutes.delete('/:userId/:taskId', findUser, findTask, (req, res) =>
  taskController.deleteTask(req, res),
);

//Listar tarefas pendentes
taskRoutes.get('/:userId', findUser, (req, res) =>
  taskController.listPendingTasks(req,res),
);

module.exports = taskRoutes;