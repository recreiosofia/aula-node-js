const Router = require('express');

const userRoutes = require('./user.routes');
const taskRoutes = require('./task.routes');

const router = Router();

router.use('/users', userRoutes);
router.use('/tasks', taskRoutes);

module.exports = router;