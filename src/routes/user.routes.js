const express = require('express');
const userRoutes = express.Router();

//Inclusão dos Middlewares
const checkUserExists = require('../middlewares/checkUserExistsMiddleware');
const findUser = require('../middlewares/FindUserMiddleware');

//Inclusão dos Controllers
const userController = require('../controllers/userController');

//Criar Usuário
userRoutes.post("/", checkUserExists, (req, res) =>
   userController.createUser(req,res),
);

//Listar Usuários
userRoutes.get("/", (req, res) => userController.listUsers(req,res));

//Buscar usuário pelo id
userRoutes.get("/:userId", findUser, (req, res) =>
   userController.getUser(req,res),
);

//Atualizar usuário
userRoutes.patch('/:userId', findUser, checkUserExists, (req, res) =>
   userController.updateUser(req, res),
);

//Tornar usuário admin
userRoutes.patch('/admin/:userId', findUser, (req, res) =>
   userController.makeUserAdmin(req,res),
);

//Deletar usuário
userRoutes.delete('/:userId', findUser, (req, res) =>
   userController.DeleteUser(req, res),
);

module.exports = userRoutes;