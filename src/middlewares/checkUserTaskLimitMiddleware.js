const { Task } = require("../models/models");

async function checkUserTaskLimit(req, res, next) {
    try {
        const { user } = req;
        const { count } = await Task.findAndCountAll({
            where: {
                UserId: user.id,
            },
        });

        if(user.isAdmin || count < 5) {
            return next();
        }

        return res.status(400).json({error: "User is not an admin and already has 5 tasks"});
    } catch(error) {
        return res.status(500).json({ error: "Erro ao ver quantidade de tarefas do usuário"})
    }
} 

module.exports = checkUserTaskLimit;