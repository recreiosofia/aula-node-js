const { User } = require("../models/models");

async function checkUserExists(req, res, next){
    try {
        const { username } = req.body;

        const userAlreadyExists = await User.findOne({
            where: {
               username, 
            },
        });

        if(userAlreadyExists){
            return res.status(400).json({error: "User already exists!"});
        };

        return next();
    } catch(error) {
        return res.status(500).json({error: "Error ao checar se usuário existe"});
    }   
}

module.exports = checkUserExists;