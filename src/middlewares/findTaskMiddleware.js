const { Task } = require("../models/models");

async function findTask(req, res, next) {
    try{
        const { taskId } = req.params;

        const task = await Task.findByPk(taskId);

        if (!task) {
            return res.status(404).json({ error: "Task not found"});
        }

        req.task = task;

        return next();
    } catch(error) {
        return res 
            .status(500)
            .json({error: "Erro ao tentar achar a tarefa"});
    }
}

module.exports = findTask;