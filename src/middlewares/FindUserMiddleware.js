const { User } = require("../models/models");

async function findUser(req, res, next) {
    try {
        const { userId } = req.params;

        const user = await User.findByPk(userId);

        if (!user) {
            return res.status(404).json({ error: "User not Found!" });
        }

        req.user = user;

        return next();
    } catch (error) {
        return res.status(500).json({
            error: "Error ao achar o usuário",
            message: error.message,
        });
    }
}

module.exports = findUser;
